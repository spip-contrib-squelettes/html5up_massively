<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'age_articles_recents' => 'Âge des articles récents',
	'age_articles_recents_explications' => 'Mettre un chiffre pour le nombre de jours : les articles plus âgés ne seront pas affichés.',
	'arriereplan' => 'Arrière-plan',
	'arriereplan_explications' => 'Choisir ici l’image d’arrière-plan si vous l’avez déjà chargée dans la médiathèque.',
	
	// C
	'cfg_titre_parametrages' => 'Paramétrages',
	'contact' => 'Restez en contact',
	'couleur_accent' => 'Accent de couleur',
	'couleur_accent_explications' => 'C’est la couleur au survol des liens. Par défaut #18bfef',
	'contact_tel' => 'Téléphone',
	'contact_adresse' => 'Adresse',
	'contact_complement' => 'Complément',
	'couleur_typo' => 'Couleur de la typographie',
	'couleur_typo_explications' => 'Par défaut #212931',
	'couleur_bkg' => 'Couleur d’arrière plan de la page',
	'couleur_bkg_explications' => 'Par défaut #FFFFFF',
	'couleur_bkg_footer' => 'Couleur d’arrière plan du pied de page',
	'couleur_bkg_footer_explications' => 'Par défaut #f6f7ea',
	'couleur_typo_footer' => 'Couleur de la typographie du pied de page',
	'couleur_typo_footer_explications' => 'Par défaut #717981',
	'couleur_typo_copyright' => 'Couleur de la typographie du menu de pied de page',
	'couleur_typo_copyright_explications' => 'Par défaut #717981',
	'couleur_bkg_texte' => 'Couleur de fond sous le contenu',
	'couleur_bkg_texte_explications' => 'Par défaut #FFFFFF',
	
	// D
	"design" => 'Design',
	
	// F
	'footer' => 'Pied de page',
	
	// L
	'largeur_page' => 'Largeur de la page',
	'largeur_page_explications' => 'Si les lignes de texte vous parraissent trop longues à la lecture, vous pouvez choisir la largeur maximum du contenu des pages (max-width). Exprimez ici la valeur et son unité (%,px,vw...).',
	
	// M
	'mode_accueil' => 'Mode de la page d\'accueil',
	'mode_accueil_blog' => 'Mode blog (derniers articles publiés)',
	'mode_accueil_explications' => 'Choisissez le mode d\'affichage de la page d\'accueil',
	'mode_accueil_site' => 'Mode site (rubriques à la racine)',

	// R
	'rechercher' => 'Chercher',
	'reseaux_sociaux' => 'Réseaux sociaux',
	'reseaux_sociaux_activer' => 'Activer le plugin «Liens vers les réseaux sociaux» pour afficher de tels liens.',
	'reseaux_sociaux_configurer' => 'Configurer le plugin : Liens sociaux',
	
	// S
	'suivre' => 'Suivre',
	
	// T
	'theme_graphique_par_html5up' => 'Thème graphique par HTML5 UP',
);
